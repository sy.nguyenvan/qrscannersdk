# QRScannerSDK

[![CI Status](https://img.shields.io/travis/sy.nguyenvan/QRScannerSDK.svg?style=flat)](https://travis-ci.org/sy.nguyenvan/QRScannerSDK)
[![Version](https://img.shields.io/cocoapods/v/QRScannerSDK.svg?style=flat)](https://cocoapods.org/pods/QRScannerSDK)
[![License](https://img.shields.io/cocoapods/l/QRScannerSDK.svg?style=flat)](https://cocoapods.org/pods/QRScannerSDK)
[![Platform](https://img.shields.io/cocoapods/p/QRScannerSDK.svg?style=flat)](https://cocoapods.org/pods/QRScannerSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

QRScannerSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'QRScannerSDK'
```

## Author

sy.nguyenvan, sy.nguyenvan@vti.com.vn

## License

QRScannerSDK is available under the MIT license. See the LICENSE file for more info.
